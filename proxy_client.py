import asyncio
import logging
import os
import sys
import webbrowser
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

if len(sys.argv) != 3:
    print("- proxy_server.py usage:")
    print(">>> python proxy_server.py [L_PORT] [S_KEY_PATH]")
    sys.exit(1)

# Read input arguments
script_path, local_port, secret_key_path = sys.argv
remote_host = 'songspotting.ddns.net'
remote_port = '7000'
destination_address = ':'.join((remote_host, remote_port))

# Setup logging
logger = logging.getLogger(__name__)
formatter = logging.Formatter('[%(asctime)s] %(l_address)-21s %(direction)s {r_address} - %(message)s'.format(
    r_address=destination_address
))
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

# Read secret key and prepare cipher
with open(secret_key_path, 'rb') as f:
    key = f.read()


async def proxy(reader, writer, direction, source_address):
    total_data = {True: 0, False: 0}
    while True:
        if direction:
            # Read at most 255 bytes, at least one
            # will be added as padding so that the
            # total packet length is 256
            data = await reader.read(255)
            if not data:
                break
            total_data[direction] += len(data)
            # Add padding
            length = 255 - len(data)
            data += bytes([length]) * (length + 1)
            # Encrypt data before sending stream
            iv = os.urandom(16)
            cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
            encryptor = cipher.encryptor()
            data = encryptor.update(data)
            data += encryptor.finalize()
            data = iv + data
            # Logging
            if total_data[direction] % (1024 * 1024) < 255:
                logger.info(
                    '{:.0f} Mb'.format(total_data[direction] / (1024 * 1024)),
                    extra={
                        'l_address': source_address,
                        'direction': '>>>'
                    }
                )

        else:
            try:
                # We need exactly 256 bytes to decrypt,
                # this is the size of the packet that
                # was encrypted
                data = await reader.readexactly(272)
                total_data[direction] += len(data)
            except asyncio.IncompleteReadError:
                break
            # Decrypt incoming stream
            iv, data = data[:16], data[16:]
            cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
            decryptor = cipher.decryptor()
            data = decryptor.update(data)
            data += decryptor.finalize()
            # Remove padding
            data = data[:-(data[-1]+1)]
            # Logging
            if total_data[direction] % (1024 * 1024) < 272:
                logger.info(
                    '{:.0f} Mb'.format(total_data[direction] / (1024 * 1024)),
                    extra={
                        'l_address': source_address,
                        'direction': '<<<'
                    }
                )

        writer.write(data)
        await writer.drain()

    writer.close()
    if direction:
        logger.info(
            'Closed connection',
            extra={
                'l_address': source_address,
                'direction': '>X<'
            }
        )
    else:
        logger.info(
            'Closed connection',
            extra={
                'l_address': source_address,
                'direction': '>X<'
            }
        )


async def handle_connection(source_reader, source_writer):
    target_reader, target_writer = await asyncio.open_connection(remote_host, remote_port)
    source_address = '{}:{}'.format(
        source_writer.get_extra_info('peername')[0],
        source_writer.get_extra_info('peername')[1]
    )
    extra = {
        'l_address': source_address,
        'direction': '<&>'
    }
    logger.info('Created connection', extra=extra)
    await asyncio.wait([
        proxy(source_reader, target_writer, True, source_address),
        proxy(target_reader, source_writer, False, source_address)
    ])


loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_connection, '', local_port, loop=loop)
server = loop.run_until_complete(coro)

logger.info(
    'Started proxy',
    extra={'l_address': 'localhost:{}'.format(local_port), 'direction': '-->'}
)

try:
    webbrowser.open('http://localhost:{}'.format(local_port))
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
logger.info(
    'Stopped proxy',
    extra={'l_address': 'localhost:{}'.format(local_port), 'direction': '-/>'}
)
